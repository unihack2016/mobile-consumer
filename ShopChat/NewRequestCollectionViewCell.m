//
//  NewRequestCollectionViewCell.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "NewRequestCollectionViewCell.h"

@interface NewRequestCollectionViewCell ()
@property (nonatomic, weak) IBOutlet UIImageView *imgNewIcon;
@end

@implementation NewRequestCollectionViewCell

- (void)awakeFromNib {
    
    self.imgNewIcon.layer.borderColor = [UIColor colorWithRed:118.0/255.0 green:85.0/255.0 blue:55.0/255.0 alpha:1].CGColor;
    self.imgNewIcon.layer.borderWidth = 0.5;
    self.imgNewIcon.layer.masksToBounds = true;
}

@end
