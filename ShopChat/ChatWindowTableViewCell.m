//
//  ChatWindowTableViewCell.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "ChatWindowTableViewCell.h"

#import <Sinch/Sinch.h>

@interface ChatWindowTableViewCell ()
@property (nonatomic, weak) IBOutlet UIView *container;
@property (nonatomic, weak) IBOutlet UILabel *lblMessage;
@end

@implementation ChatWindowTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.container.layer.borderWidth = 0.5;
    self.container.layer.cornerRadius = 12;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)populateWithMessage:(NSDictionary *)message {
    
    if ([message[@"message"] isKindOfClass:[NSString class]])
        self.lblMessage.text = message[@"message"];
    else {
        id<SINMessage> sinMessage = message[@"message"];
        self.lblMessage.text = sinMessage.text;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        float width = self.frame.size.width;
        
        if ([message[@"user"] isEqualToString:@"me"]) {
            self.container.frame = CGRectMake(width/3.0, 8, width/3.0*2.0-16, 42);
            self.container.layer.borderColor = [UIColor colorWithRed:118.0/255.0 green:85.0/255.0 blue:55.0/255.0 alpha:1].CGColor;
        } else {
            self.container.frame = CGRectMake(16, 8, width/3.0*2.0-16, 42);
            self.container.layer.borderColor = [UIColor blackColor].CGColor;
        }
        
        
        self.lblMessage.frame = CGRectMake(self.lblMessage.frame.origin.x, self.lblMessage.frame.origin.y, self.container.frame.size.width-16, self.container.frame.size.height);
    });
    
}

@end
