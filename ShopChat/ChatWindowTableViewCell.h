//
//  ChatWindowTableViewCell.h
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatWindowTableViewCell : UITableViewCell

- (void)populateWithMessage:(NSDictionary *)message;

@end
