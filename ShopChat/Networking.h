//
//  Networking.h
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const BASE_URL;

@interface Networking : NSObject

+ (Networking *)shared;

- (void)postNewShoppingRequest:(NSDictionary *)shoppingRequest completion:(void (^)(NSDictionary *result))completion;

@end
