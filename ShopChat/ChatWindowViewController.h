//
//  ChatWindowViewController.h
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatWindowViewController : UIViewController

- (void)populateWithChat:(NSDictionary *)chat;

@end
