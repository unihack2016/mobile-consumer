//
//  AppDelegate.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate () <SINClientDelegate, SINMessageClientDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self initSinchClientWithUserId:@"Dan"];
     
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)presentAlertMessageWithTitle:(NSString *)title message:(NSString *)message {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:title message:message delegate:self.window.rootViewController cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    });
}
     
#pragma chat client
- (void)initSinchClientWithUserId:(NSString *)userId {
    self.username = userId;
    if (!_client) {
        _client = [Sinch clientWithApplicationKey:@"784b4f30-c51d-4015-9c6a-245e4e671307"
                             applicationSecret:@"H4a97nnxbk2PstNc3kOUmg=="
                               environmentHost:@"sandbox.sinch.com"
                                        userId:userId];

        _client.delegate = self;

        [_client setSupportMessaging:YES];

        [_client start];
        [_client startListeningOnActiveConnection];
    }
}

- (void)sendMessage:(NSString *)text toRecipient:(NSString *)recipient {
    
    if (!recipient || recipient.length == 0 || !text || text.length == 0)
        return;
    
    SINOutgoingMessage *message = [SINOutgoingMessage messageWithRecipient:recipient text:text];
    
    [[self.client messageClient] sendMessage:message];
}

#pragma mark - SINClientDelegate
     
- (void)clientDidStart:(id<SINClient>)client {
     NSLog(@"Sinch client started successfully (version: %@)", [Sinch version]);
    [self.client messageClient].delegate = self;
    
    
 }
 
 - (void)clientDidFail:(id<SINClient>)client error:(NSError *)error {
     NSLog(@"Sinch client error: %@", [error localizedDescription]);
 }
 
 - (void)client:(id<SINClient>)client logMessage:(NSString *)message area:(NSString *)area severity:(SINLogSeverity)severity timestamp:(NSDate *)timestamp {
    if (severity == SINLogSeverityCritical) {
        NSLog(@"%@", message);
    }
}

#pragma mark - SINMessageClientDelegate

- (void)messageClient:(id<SINMessageClient>)messageClient didReceiveIncomingMessage:(id<SINMessage>)message {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"receivedMessage" object:nil userInfo:@{@"message":message}];
}

- (void)messageSent:(id<SINMessage>)message recipientId:(NSString *)recipientId {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sentMessage" object:nil userInfo:@{@"message":message}];
}

- (void)messageDelivered:(id<SINMessageDeliveryInfo>)info {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deliveredMessage" object:nil userInfo:@{@"info":info}];
    
    NSLog(@"Message to %@ was successfully delivered", info.recipientId);
}

- (void)messageFailed:(id<SINMessage>)message info:(id<SINMessageFailureInfo>)failureInfo {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"failedMessage" object:nil userInfo:@{@"info":failureInfo}];
}

@end
