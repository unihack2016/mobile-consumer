//
//  NewRequestCollectionViewController.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "NewRequestCollectionViewController.h"
#import "RequestCollectionViewCell.h"
#import "ChatsOpenCollectionViewController.h"
#import "CreateNewRequestViewController.h"

#import "MZFormSheetController.h"

@interface NewRequestCollectionViewController () <CreateNewRequestViewControllerDelegate>
@property (nonatomic, strong) NSMutableArray *requests;
@end

@implementation NewRequestCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.requests = @[
                      @{@"title":@"Apparel",
                        @"description":@"checkered office shirt in size 40",
                        @"badge":@"2",
                        @"status":@"Open"
                          },
                      @{@"title":@"Shoes",
                        @"description":@"black runners size 13",
                        @"badge":@"0",
                        @"status":@"Open"
                        },
                      @{@"title":@"Books",
                        @"description":@"robin cooks collection",
                        @"badge":@"0",
                        @"status":@"Open"
                        },
                      @{@"title":@"Sport",
                        @"description":@"top end cricket bat",
                        @"badge":@"0",
                        @"status":@"Open"
                        }
                      ].mutableCopy;
    
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.requests.count+1;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float width = (self.view.frame.size.width-33)/3.0;
    
    return CGSizeMake(width, width);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        return [collectionView dequeueReusableCellWithReuseIdentifier:@"NewRequestCell" forIndexPath:indexPath];
    }
    
    RequestCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RequestCell" forIndexPath:indexPath];
    
    [cell populateWithRequest:self.requests[indexPath.row-1]];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:true];
    
    NSLog(@"tapped: %i", (int)indexPath.row);
    // move to next page
    
    if (indexPath.row == 0) {
        // new request

        UINavigationController *nvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CreateNewRequestViewController"];
        
        CreateNewRequestViewController *vc = (CreateNewRequestViewController*)[nvc topViewController];
        vc.delegate = self;
        
//        CreateNewRequestViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CreateNewRequestViewController"];
        MZFormSheetController *mz = [[MZFormSheetController alloc] initWithSize:CGSizeMake(280, 298) viewController:nvc];
        mz.transitionStyle = MZFormSheetTransitionStyleDropDown;
        mz.shouldDismissOnBackgroundViewTap = true;
        
        [self mz_presentFormSheetController:mz animated:true completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
            NSLog(@"done!");
        }];
        
//        UINavigationController *nvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CreateNewRequestViewController"];
//        [self presentViewController:nvc animated:true completion:nil];
    } else {
        ChatsOpenCollectionViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChatsOpenCollectionViewController"];
        [vc populateWithRequest:self.requests[indexPath.row-1]];
        [self.navigationController pushViewController:vc animated:true];
    }
}

- (void)didCreateRequest:(NSDictionary *)request {
    
    [self.requests addObject:@{@"title":request[@"category"],@"description":request[@"description"]}];
    [self.collectionView reloadData];
}

@end
