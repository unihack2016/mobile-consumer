//
//  AppDelegate.h
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>

#define app_delegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) id<SINClient> client;
@property (nonatomic, strong) NSString *username;

- (void)presentAlertMessageWithTitle:(NSString *)title message:(NSString *)message;
- (void)sendMessage:(NSString *)text toRecipient:(NSString *)recipient;

@end

