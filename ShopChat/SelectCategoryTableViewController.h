//
//  SelectCategoryTableViewController.h
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 31/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectCategoryTableViewControllerDelegate <NSObject>
- (void)didSelectCategory:(NSString *)category;
@end

@interface SelectCategoryTableViewController : UITableViewController
@property (nonatomic, weak) id<SelectCategoryTableViewControllerDelegate>delegate;
@end
