//
//  Networking.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "Networking.h"
#import "AppDelegate.h"

NSString * const BASE_URL = @"https://shophack.herokuapp.com/api/request/";
//NSString * const BASE_URL = @"http://10.21.142.146:3000/api/request/";

@interface Networking () <NSURLSessionDelegate>
@property (nonatomic, strong) NSURLSession *session;
@end

@implementation Networking

+ (Networking *)shared {
    // Structure used to test whether the block has completed or not
    static dispatch_once_t completed = 0;
    
    // Initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // Executes a block object once and only once for the lifetime of an application
    dispatch_once(&completed, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject setupSession];
    });
    
    return _sharedObject;
}

- (void)setupSession {
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"Content-Type": @"application/json"};
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
}

- (void)handlErrorResponse:(NSURLResponse *)response json:(NSDictionary *)json message:(NSString *)message {
    
    if (message)
    {
        [app_delegate presentAlertMessageWithTitle:@"Whoops!" message:message];
        NSLog(@"%@", message);
    }
    
    if (response)
        NSLog(@"Response: %@", response);
    
    if (json)
        NSLog(@"JSON: %@", json);
    
}


- (void)postNewShoppingRequest:(NSDictionary *)shoppingRequest completion:(void (^)(NSDictionary *result))completion {
    
    // Create up a id string of clubs
//    NSString *requestsString = @"";
//    for (UVClub *club in clubs)
//    {
//        if (club == [clubs firstObject])
//            clubsString = club.identifier;
//        else
//            clubsString = [NSString stringWithFormat:@"%@,%@", clubsString, club.identifier];
//    }
    
    [self apiPOST:@"new" parameters:shoppingRequest success:^(NSDictionary *responseJson) {
        
        // TODO: something with the return value
        completion(responseJson);
        
    } failure:^(NSDictionary *responseJson, NSURLResponse *response) {
        
        [self handlErrorResponse:response json:responseJson message:@"POST subscribed clubs failed"];
        
        if (completion)
            completion(nil);
    }];
}

#pragma mark - Network API request functions

- (void)apiGET:(NSString *)urlPath success:(void (^)(NSDictionary *responseJson))success failure:(void (^)(NSDictionary *responseJson, NSURLResponse *response))failure {
    
    // Create the full url inlcuding the base and v. num
    NSURL *fullURL = [NSURL URLWithString:[self createURLFromPath:urlPath]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:fullURL];
    request.HTTPMethod = @"GET";
    
    // Perform the GET request
    NSURLSessionTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        if (data)
        {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (json[@"results"])
            {
                success(json);
            }
            else
            {
                failure(json, response);
            }
        }
        else
        {
            failure(nil, response);
        }
        
    }];
    
    [task resume];
}

- (void)apiPOST:(NSString *)urlPath parameters:(NSDictionary *)parameters success:(void (^)(NSDictionary *responseJson))success failure:(void (^)(NSDictionary *responseJson, NSURLResponse *response))failure {
    
    NSString *bodyString;
    for (int i = 0; i < [parameters count]; i++)
    {
        if (i == 0)
        {
            bodyString = [NSString stringWithFormat:@"%@=%@", [parameters allKeys][i], [parameters allValues][i]];
        }
        else
        {
            bodyString = [NSString stringWithFormat:@"%@,%@=%@", bodyString, [parameters allKeys][i], [parameters allValues][i]];
        }
    }
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    
    // Create the full url inlcuding the base and v. num
    NSURL *fullURL = [NSURL URLWithString:[self createURLFromPath:urlPath]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:fullURL];
    request.HTTPMethod = @"POST";
    request.HTTPBody = jsonData;
    
    // Perform the GET request
    NSURLSessionTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
        if (data)
        {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (json[@"results"])
            {
                success(json);
            }
            else if (json[@"detail"])
            {
                failure(json, response);
            }
            else
            {
                success(json);
            }
        }
        else
        {
            failure(nil, response);
        }
        
    }];
    [task resume];
}

#pragma mark - URL Tools

- (NSString *)addParameters:(NSDictionary *)parameters toURLString:(NSString *)urlString {
    
    // Append slash if URL doesn't already finish with one (fixes iOS 9 missing headers bug)
    if ([urlString characterAtIndex:([urlString length]-1)] != '/')
    {
        urlString = [NSString stringWithFormat:@"%@/", urlString];
    }
    
    // Add the parameters to the partial/full url inlcuding parameters
    NSURL *fullURL = [NSURL URLWithString:urlString];
    NSURLComponents *urlComponents = [[NSURLComponents alloc] initWithURL:fullURL resolvingAgainstBaseURL:false];
    
    NSMutableArray *queryItems = [NSMutableArray array];
    for (NSString *key in parameters.allKeys)
    {
        [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:parameters[key]]];
    }
    urlComponents.queryItems = queryItems;
    
    return urlComponents.URL.absoluteString;
}

- (NSString *)encodeURLString:(NSString *)stringToEncode {
    
    return [stringToEncode stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
}

- (NSString *)createURLFromPath:(NSString *)urlPath {
    
    return [NSString stringWithFormat:@"%@%@", BASE_URL, urlPath];
}

@end
