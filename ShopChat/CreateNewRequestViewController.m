//
//  CreateNewRequestViewController.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 31/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "CreateNewRequestViewController.h"
#import "SelectCategoryTableViewController.h"
#import "MZFormSheetController.h"
#import "AppDelegate.h"
#import "Networking.h"

#import "MBProgressHUD.h"

@interface CreateNewRequestViewController () <SelectCategoryTableViewControllerDelegate>
@property (nonatomic, weak) IBOutlet UIView *container;
@property (nonatomic, weak) IBOutlet UITextView *txtDescription;
@property (nonatomic, weak) IBOutlet UIButton *btnCategory;
@end

@implementation CreateNewRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.container.layer.borderColor = [UIColor colorWithRed:118.0/255.0 green:85.0/255.0 blue:55.0/255.0 alpha:1].CGColor;
    self.container.layer.borderWidth = 0.5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnChooseCategoryTapped:(id)sender {
        
    SelectCategoryTableViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectCategoryTableViewController"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)post:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    
    [[Networking shared] postNewShoppingRequest:@{@"username":app_delegate.username,@"description":self.txtDescription.text,@"category":self.btnCategory.titleLabel.text} completion:^(NSDictionary *result) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:true];
            
            if (!result) {
                NSLog(@"error!");
            } else {
                NSLog(@"yay");
                [self mz_dismissFormSheetControllerAnimated:true completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
                    
                }];
                [self.delegate didCreateRequest:@{@"description":self.txtDescription.text,@"category":self.btnCategory.titleLabel.text}];
            }        });
        
    }];
}

- (IBAction)cancel:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:true completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
        
    }];
}

- (void)didSelectCategory:(NSString *)category {
    
    [self.btnCategory setTitle:category forState:UIControlStateNormal];
}

@end
