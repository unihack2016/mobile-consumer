//
//  NewRequestCollectionViewCell.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "RequestCollectionViewCell.h"

@interface RequestCollectionViewCell ()
@property (nonatomic, weak) IBOutlet UIView *requestView;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblDescription;
@property (nonatomic, strong) UILabel *lblBadge;
@property (nonatomic, strong) UIView *seperator;
@end

@implementation RequestCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.requestView.layer.borderColor = [UIColor colorWithRed:118.0/255.0 green:85.0/255.0 blue:55.0/255.0 alpha:1].CGColor;
    self.requestView.layer.borderWidth = 0.5;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self.seperator = [[UIView alloc] initWithFrame:CGRectMake(self.lblTitle.frame.origin.x-2, self.lblTitle.frame.origin.y+self.lblTitle.frame.size.height+2, self.lblTitle.frame.size.width+4, 0.5)];
        self.seperator.backgroundColor = [UIColor colorWithRed:84.0/255.0 green:53.0/255.0 blue:28.0/255.0 alpha:1];
        [self.requestView addSubview:self.seperator];
    });
}

- (void)populateWithRequest:(NSDictionary *)request {
    
    if (!self.lblBadge) {
        self.lblBadge = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.lblBadge.backgroundColor = [UIColor blackColor];
        self.lblBadge.textColor = [UIColor whiteColor];
        self.lblBadge.textAlignment = NSTextAlignmentCenter;
        self.lblBadge.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
        [self addSubview:self.lblBadge];
    }
    
    
    self.lblTitle.text = request[@"title"];
    self.lblDescription.text = request[@"description"];
    self.lblBadge.text = request[@"badge"];
    
    if (request[@"badge"] && [(NSString *)request[@"badge"] intValue] > 0) {
        self.lblBadge.hidden = false;
    } else {
        self.lblBadge.hidden = true;
    }
    
    
    // wait for ui to load
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.lblBadge.frame = CGRectMake(0, 0, 20, 20);
        self.lblBadge.layer.cornerRadius = 10;
        self.lblBadge.layer.masksToBounds = true;
        self.lblBadge.center = CGPointMake(self.requestView.frame.origin.x+self.requestView.frame.size.width, self.requestView.frame.origin.y);
        
        
        // Return the size the text label should be
        if ([self.lblDescription sizeThatFits:CGSizeMake(self.requestView.frame.size.width-8, self.requestView.frame.size.height-self.lblDescription.frame.origin.y-4)].height < self.lblDescription.frame.size.height) {
            
            CGSize textSize = [self.lblDescription sizeThatFits:CGSizeMake(self.requestView.frame.size.width-8, self.requestView.frame.size.height-self.lblDescription.frame.origin.y-4)];
            self.lblDescription.frame = CGRectMake(self.lblDescription.frame.origin.x, self.lblDescription.frame.origin.y, textSize.width, textSize.height);
        }
        
    });
    

}

@end
