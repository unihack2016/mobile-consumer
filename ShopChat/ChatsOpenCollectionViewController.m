//
//  ChatsOpenCollectionViewController.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "ChatsOpenCollectionViewController.h"
#import "ChatsOpenCollectionViewCell.h"
#import "ChatWindowViewController.h"

@interface ChatsOpenCollectionViewController () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *lblCategory;
@property (nonatomic, weak) IBOutlet UILabel *lblDescription;
@property (nonatomic, weak) IBOutlet UIView *headerDetails;
@property (nonatomic, strong) NSDictionary *request;
@property (nonatomic, strong) NSMutableArray *chats;
@end

@implementation ChatsOpenCollectionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    self.lblCategory.text = self.request[@"title"];
    self.lblDescription.text = self.request[@"description"];
    
    self.chats = @[
                   @{@"storeUsername":@"Myer",
                    @"imageName":@"myer"
                       },
                   @{@"storeUsername":@"JJ",
                     @"imageName":@"justjeans"
                     },
                   @{
                       @"storeUsername":@"Bardot",
                       @"imageName":@"bardot"
                       }
                   ].mutableCopy;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        // determin header height
//        CGFloat width = [UIScreen mainScreen].bounds.size.width;
//        CGSize textSize = [self.lblDescription sizeThatFits:CGSizeMake(width-64, 20000.0)];
//        
//        self.headerDetails.frame = CGRectMake(self.headerDetails.frame.origin.x, self.headerDetails.frame.origin.y, width, self.headerDetails.frame.size.height-self.lblDescription.frame.size.height+textSize.height);
//        self.lblDescription.frame = CGRectMake(self.lblDescription.frame.origin.x, self.lblDescription.frame.origin.y, textSize.width, textSize.height);
//        
//        [self updateViewConstraints];
//    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateWithRequest:(NSDictionary *)request {
    
    self.request = request;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.chats.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float width = (self.view.frame.size.width-33)/3.0;
    float height = width*(185.0/150.0);
    
    return CGSizeMake(width, height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatsOpenCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatCell" forIndexPath:indexPath];
        
    [cell populateWithChat:self.chats[indexPath.row]];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:true];
    
    // move to chat window
    ChatWindowViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChatWindowViewController"];
    [vc populateWithChat:self.chats[indexPath.row]];
    [self.navigationController pushViewController:vc animated:true];
}

@end
