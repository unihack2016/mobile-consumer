//
//  ChatWindowViewController.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "ChatWindowViewController.h"
#import "ChatWindowTableViewCell.h"
#import "AppDelegate.h"

#import <Sinch/Sinch.h>
#import "MBProgressHUD.h"

@interface ChatWindowViewController () <UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *messageBar;
@property (nonatomic, weak) IBOutlet UITextField *txtMessage;
@property (nonatomic, weak) IBOutlet UIButton *btnSendMessage;
@property (nonatomic, strong) UIView *hiddenMessageBar;
@property (nonatomic, strong) UITextField *txtHiddenMessage;
@property (nonatomic, strong) UIButton *btnHiddenSendMessage;
@property (nonatomic, strong) UIActivityIndicatorView *sendingMessageIndicator;

@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) NSDictionary *chatInfo;
@property (nonatomic, strong) NSString *recepientUsername;
@end

@implementation ChatWindowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.dataSource = self;
    
    self.messages = @[@{@"message":@"Hi! We've got this in stock now, but there's only three left",
                      @"user":@"you"},
                      @{@"message":@"Do you want me to hold it for you?",
                        @"user":@"you"}
                      ].mutableCopy;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeFirstResponder) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"receivedMessage" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        if (note && note.userInfo) {
            id<SINMessage> message = note.userInfo[@"message"];
            
            if (![message.senderId isEqualToString:self.recepientUsername])
                return;
            
            NSDictionary *messageData = @{
                                      @"message":note.userInfo[@"message"],
                                      @"user":@"you"
                                      };
            
            [self.messages addObject:messageData];
            [self.tableView reloadData];
            [self scrollToBottom];
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"sentMessage" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        if (note && note.userInfo) {
            id<SINMessage> message = note.userInfo[@"message"];
            
            if (![message.recipientIds containsObject:self.recepientUsername])
                return;
            
            self.sendingMessageIndicator.hidden = true;
            self.btnHiddenSendMessage.hidden = false;
            NSDictionary *messageData = @{
                                      @"message":message,
                                      @"user":@"me"
                                      };
            
            [self.messages addObject:messageData];
            [self.tableView reloadData];
            [self scrollToBottom];
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"deliveredMessage" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        if (note && note.userInfo) {
//            id<SINMessage> message = note.userInfo[@"message"];
            
            
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"failedMessage" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        if (note && note.userInfo) {
//            id<SINMessage> message = note.userInfo[@"message"];
            
            
            self.sendingMessageIndicator.hidden = true;
            self.btnHiddenSendMessage.hidden = false;
        }
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.hiddenMessageBar = [[UIView alloc] initWithFrame:self.messageBar.frame];
    self.hiddenMessageBar.backgroundColor = [UIColor whiteColor];// [UIColor colorWithRed:118.0/255.0 green:85.0/255.0 blue:55.0/255.0 alpha:1];
    self.txtHiddenMessage = [[UITextField alloc] initWithFrame:self.txtMessage.frame];
    self.txtHiddenMessage.borderStyle = UITextBorderStyleRoundedRect;
    self.btnHiddenSendMessage = [[UIButton alloc] initWithFrame:self.btnSendMessage.frame];
    [self.btnHiddenSendMessage setTitleColor:[UIColor colorWithRed:118.0/255.0 green:85.0/255.0 blue:55.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.btnHiddenSendMessage setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [self.btnHiddenSendMessage setTitle:@"Send" forState:UIControlStateNormal];
    [self.btnHiddenSendMessage addTarget:self action:@selector(sendTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.btnHiddenSendMessage.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    
    UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.hiddenMessageBar.frame.size.width, 0.5)];
    seperator.backgroundColor = [UIColor colorWithRed:118.0/255.0 green:85.0/255.0 blue:55.0/255.0 alpha:1];
    [self.hiddenMessageBar addSubview:seperator];
    
    [self.hiddenMessageBar addSubview:self.txtHiddenMessage];
    [self.hiddenMessageBar addSubview:self.btnHiddenSendMessage];
    
    self.txtMessage.inputAccessoryView = self.hiddenMessageBar;
    
    [self.txtMessage becomeFirstResponder];
    
    self.sendingMessageIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.sendingMessageIndicator startAnimating];
    self.sendingMessageIndicator.center = self.btnHiddenSendMessage.center;
    [self.hiddenMessageBar addSubview:self.sendingMessageIndicator];
    self.sendingMessageIndicator.hidden = true;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeFirstResponder {
    [self.txtHiddenMessage becomeFirstResponder];
}

- (void)populateWithChat:(NSDictionary *)chat {
    
    self.chatInfo = chat;
    self.recepientUsername = chat[@"storeUsername"];
}

- (IBAction)sendTapped:(id)sender {
    
    if (self.txtHiddenMessage.text.length == 0)
        return;
    
    self.btnHiddenSendMessage.hidden = true;
    self.sendingMessageIndicator.hidden = false;
    
    [app_delegate sendMessage:self.txtHiddenMessage.text toRecipient:self.recepientUsername];
    self.txtHiddenMessage.text = @"";
}

- (void)scrollToBottom {

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(self.messages.count - 1) inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatWindowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatMessageCell"];
    
    [cell populateWithMessage:self.messages[indexPath.row]];
    
    return cell;
}

@end
