//
//  ChatsOpenCollectionViewCell.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 30/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "ChatsOpenCollectionViewCell.h"

@interface ChatsOpenCollectionViewCell ()
@property (nonatomic, weak) IBOutlet UIImageView *imgPicture;
@property (nonatomic, weak) IBOutlet UILabel *lblStore;
@end

@implementation ChatsOpenCollectionViewCell

- (void)awakeFromNib {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.imgPicture.layer.cornerRadius = self.imgPicture.frame.size.width/2;
        self.imgPicture.layer.masksToBounds = true;
        self.imgPicture.layer.borderColor = [UIColor colorWithRed:118.0/255.0 green:85.0/255.0 blue:55.0/255.0 alpha:1].CGColor;
        self.imgPicture.layer.borderWidth = 0.5;
    });
    
}

- (void)populateWithChat:(NSDictionary *)chat {
    
    self.imgPicture.image = [UIImage imageNamed:chat[@"imageName"]];
    self.lblStore.text = chat[@"storeUsername"];
}

@end
