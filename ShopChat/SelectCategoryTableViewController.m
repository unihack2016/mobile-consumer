//
//  SelectCategoryTableViewController.m
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 31/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import "SelectCategoryTableViewController.h"

@interface SelectCategoryTableViewController ()
@property (nonatomic, strong) NSArray *categories;
@end
//SelectCategoryTableViewControllerDelegate
@implementation SelectCategoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.categories = @[@"Bags & Luggage",@"Books & DVDs",@"Electronics & Communication",@"Homeware & Furniture",@"Men's Fasion",@"Women's Fashion",@"Shoes & Footware",@"Sport & Recreation",@"Stationary & Craft",@"Car & Auto",@"Health & Beauty"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.categories.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.categories[indexPath.row];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.delegate didSelectCategory:self.categories[indexPath.row]];
    [self.navigationController popViewControllerAnimated:true];
}

@end
