//
//  CreateNewRequestViewController.h
//  ShopChat
//
//  Created by Daniel Sykes-Turner on 31/07/2016.
//  Copyright © 2016 Shoppers. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CreateNewRequestViewControllerDelegate <NSObject>
- (void)didCreateRequest:(NSDictionary *)request;
@end

@interface CreateNewRequestViewController : UIViewController
@property (nonatomic, weak) id<CreateNewRequestViewControllerDelegate>delegate;
@end
