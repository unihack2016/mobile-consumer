## PocketShop for Consumers (iOS)

This project has been build in Objective-C on Xcode 7.3.1

It makes use of Cocoapods to install third party libraries and functions. The installed libraries are:

* SinchRTC (messaging SDK)
* MBProgressHUD (shows nice loading signs)
* TPKeyboardAvoiding (when the keyboard appears, shift the screen so that all content is visible)